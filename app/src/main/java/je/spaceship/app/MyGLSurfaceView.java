package je.spaceship.app;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;

import je.spaceship.app.Objects.Ship;


public class MyGLSurfaceView extends GLSurfaceView {

    private final MyGLRenderer mRenderer;

    public MyGLSurfaceView(Context context) {
        super(context);

        // Create an OpenGL ES 2.0 context.
        setEGLContextClientVersion(2);

        // Set the Renderer for drawing on the GLSurfaceView
        mRenderer = new MyGLRenderer();
        setRenderer(mRenderer);

        // Render the view only when there is a change in the drawing data
        setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);
    }


    @Override
    public boolean onTouchEvent(MotionEvent e) {

        //TODO rewrite this method

        int index = e.getActionIndex();

        float x = e.getX(index ) / getWidth();
        float y = e.getY(index ) / getHeight();

        float dx = x * 2 - 1f;
        float dy = - ( y * 2 - 1f );

        Log.e("X", dx + " " + dy + " " + isTouchFire(dx, dy) );

        if ((e.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_POINTER_DOWN ||
                (e.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_DOWN )  {
            if (isTouchFire(dx, dy)) {
                mRenderer.fire();
            } else {
                mRenderer.moveShip(dx, dy);
            }
        } else {
            if ((e.getAction() & MotionEvent.ACTION_MASK) == MotionEvent.ACTION_MOVE) {
                if (!isTouchFire(dx, dy)) {
                    mRenderer.moveShip(dx, dy);
                }
            }
        }

        requestRender();

        return true;
    }

    private boolean isTouchFire(float x, float y) {
        return Math.pow((x - 1), 2) + Math.pow((y + 1), 2) <= Math.pow(Ship.FIRE_ZONE, 2);
    }
}